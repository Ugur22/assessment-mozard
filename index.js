const express = require('express')


const app = express()
const port = 3000

// opdracht 6
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

//opdracht 2
app.get('/', (req, res) => {

	let user = {
		name: "Ugur Ertas",
		type: "Assessment"
	}
	res.send(user);
})

//opdracht 3
app.get('/now', (req, res) => {

	let datetime = new Date();
	let currentDate = {
		date: datetime.toISOString().slice(0, 16)
	}
	res.send(currentDate);
})

//opdracht 4
app.get('/layout', (req, res) => {

	res.render('index');
})

//opdracht 5
app.get('/useragent', (req, res) => {
	let userAgent = req.get('User-Agent').split(/(\([^\)]+\)|\S+|\s+)/);
	userAgent = userAgent.filter(e => String(e).trim());

	res.render('useragent', {
		agent: userAgent
	});
})

app.listen(port, () => {
	console.log(`webapp listening at http://localhost:${port}`)
})